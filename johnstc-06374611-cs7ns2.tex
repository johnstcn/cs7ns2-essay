\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{url}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Low-Power Wide Area Networking: Technology and Irish Context\\
  {\footnotesize CS7NS2 Individual Assignment}
} 

\author{\IEEEauthorblockN{Cian Johnston}
  \IEEEauthorblockA{
    \textit{School of Computer Science and Statistics, Trinity College, Dublin, Ireland} \\
    Email: johnstc@tcd.ie}
}
\maketitle

\begin{abstract}
There are two main commercially available IoT networks in Ireland at present --- Vodafone's NB-IoT network and Vizor Technology's Sigfox network. Numerous smaller LoRa deployments also exist, such as the CONNECT centre's Pervasive Nation network. However, these developments do not exhibit the same breadth of coverage or usage as the aforementioned commercial networks.
\end{abstract}

\begin{IEEEkeywords}
iot, lpwan, lora, nb-iot, sigfox, ireland
\end{IEEEkeywords}

\section{Introduction}
\label{section1}
In recent years, the Internet of Things (IoT) has garnered considerable interest in both academic and commercial circles. The European Research Cluster on the Internet of Things (IERC) defines IoT as a ``dynamic global network infrastructure with self-configuring capabilities based on standard and interoperable communication protocols'' \cite{vermesan_europes_2012}. A number of novel commercial use cases have been proposed for IoT, such as smart cities, smart agriculture, retail, and logistics \cite{porkodi_internet_2014}. Accordingly, there is significant commercial activity in this sector, with some market analyses predicting spends in the hundreds of billions of dollars \cite{hunke2017winning}.

In support of these use cases, a suitable communications protocol is required to enable IoT devices to communicate effectively. These can broadly be divided into two categories: long-range and short-range. This paper focuses on long-range communications protocols, commonly referred to as a Low-Power Wide Area Network (LP-WAN). In particular, this paper focuses on the Irish context of LP-WAN deployments. Examples of such protocols include LoRaWAN, Sigfox, and NB-IoT, all of which have been deployed in the Republic of Ireland at the time of writing.

The remainder of this paper is divided as follows:
\begin{itemize}
  \item{
    Section \ref{section2} gives a brief overview of the aforementioned major LP-WAN standards deployed in Ireland.
  }
  \item{
    Section \ref{section3} discusses some existing use cases of LP-WANs in Ireland.
  }  
  \item {
    Finally, Section \ref{section4} presents a summary of findings.
  }
\end{itemize}

\section{Major LP-WAN Standards Available in Ireland}
\label{section2}

IoT devices generally have the following in common: they are relatively low-powered, inexpensive, and may use batteries as a power source in combination with environmental energy recovery mechanisms, such as solar power. They may also be deployed in a wide range of environments, ranging from urban areas to remote rural areas, and potentially covering a wide geographical area. LP-WANs used by these devices therefore reflect these use-cases \cite{mekki_overview_2018}:
\begin{itemize}
\item{Range exceeding 1km in urban settings, and exceeding 10km in rural settings}
\item{Low power requirement}
\item{Star network topology}
\item{Relatively low bandwidth}
\end{itemize}

Short overviews follow of the three major LP-WAN standards deployed in Ireland at the time of writing: LoRaWAN, Sigfox, and NB-IoT (Narrow-Band Internet of Things). Note that this is not an exhaustive list.

\subsection{LoRaWAN}
\label{section2.1}

LoRaWAN \footnote{LoRa refers to the physical-layer (PHY) LoRa protocol. LoRaWAN refers to the Medium Access Control (MAC) protocol used by LoRa.\cite{lora_alliance_wi-fi_2019}} is a proprietary communications protocol standardised by the LoRa-Alliance. The technology was acquired by Semtech Corporation in 2012 following the acquisition of Cycleo SAS \cite{semtech_sec_2013}. Its major characteristics include \cite{sanchez-iborra_state_2016}:
\begin{itemize}
\item{Usage of unlicensed sub-GHz bands}
\item{Chirp-Spread-Spectrum (CSS) modulation with variable spread factor}
\item{Data rates ranging from 300 bps to 50 kbps, with a maximum message payload of 243 bytes}
\item{Redundant multicast transmission from user equipment (UE) to base stations (BS)}
\item{Physical-layer security using AES encryption}
\end{itemize}

LoRa also includes support for different ``device classes'', each offering different power consumption and US/DS bandwidth characteristics depending on the required duty cycle of a given device. Additionally, a significant point in LoRa's favour is its provision for ``private networks'', where one may set up their own private LoRa network for exclusive use \cite{mekki_overview_2018}. The low hardware costs and lack of required spectrum make LoRa an attractive proposition for organisations or individuals interested in setting up their own LP-WAN.

A LoRaWAN network named Pervasive Nation is available for use in Ireland as a result of a collaboration between Semtech Corporation and CONNECT (Science Foundation Ireland Research Center for Future Networks and Communications) \cite{semtech_lora_ireland_2016}. Payment of a fee and acceptance of a user agreement is required for access to this network. More information on this network is available at \texttt{\url{https://connectcentre.ie/pervasive-nation/}}. Coverage data for this network was not readily available at time of writing.

An alternative LoRaWAN entrypoint exists via The Things Network (\texttt{\url{https://thethingsnetwork.com}}). This provider operates a collaborative LoRaWAN network that is free of charge at time of writing. Additionally, users of this network may deploy additional gateways to the public network to enhance coverage, with the provision that other members will also have access. Coverage data is available on the company's website --- however, coverage of this particular network is mainly limited to urban areas at the time of writing.

\subsection{Sigfox}
\label{section2.2}

Sigfox is a proprietary protocol developed by the France-based Sigfox SA. Sigfox differs significantly from LoRa in multiple respects \cite{zuniga_sigfox_2016}:
\begin{itemize}
\item{Uses ultra-narrowband modulation with Differential Binary Phase-Shift Keying (DBPSK) instead of spread-spectrum channels.}
\item{Limits upstream to 100 bps and maximum message payload to 12 bytes. Downstream limited to 600 bps.}
\item{Permits ranges of up to 10 km.}
\end{itemize}

Further restrictions are placed on daily usage:
\begin{itemize}
\item{Maximum 140 US messages per day, with 1\% duty cycle.}
\item{Maximum 4 DS messages per day, with 10\% duty cycle.}
\end{itemize}

Additionally, it provides no physical layer or transport layer security, requiring encryption at application level \cite{zuniga_sigfox_2016}. 
However, it does share LoRa's familiar star topology, with multiple UE communicating to multiple BS. Additionally, it uses unlicensed ISM bands in each operating region \cite{mekki_overview_2018}.

Sigfox partner with indvidual operators to roll out their network; in Ireland they have partnered with Vizor Technology, Ltd. (VT) \cite{vt_website} as a local network operator. There is scant information provided by Sigfox or their local operator VT on pricing, and also requires proprietary hardware to access. However, is appears by far to be the most mature LP-WAN network rollout available at the time of writing.

\subsection{NB-IoT}
\label{section2.3}

NB-IoT is a slightly newer arrival to the LP-WAN scene, having been finalised in 3GPP Release 13. In contrast to both LoRa and Sigfox, this is an open standard based on existing LTE standards. 3GPP claims the following \cite{ratasuk2016nb}:
\begin{itemize}
\item{Maximum DS/US data rates of 200 kbps and 20 kbps respectively.}
\item{Supports up to 1600 bytes payload per message.}
\item{Uses existing licensed spectrum, thereby making it attractive for existing network operators to roll out.}
\item{Provides guarantees on quality of service (QoS) and latency.}
\end{itemize}

Additionally, it supports multiple distinct modes of operation, allowing attractive flexibility to network operators:
\begin{itemize}
  \item{\textit{standalone operation mode}, using one GSM channel (200 kHz).}
  \item{\textit{guard band operation mode}, occupying unused resource blocks in the carrier's guard band.}
  \item{\textit{in-band operation mode}, occupying one physical LTE resource block (180 kHz).}
\end{itemize} 

A major touted advantage of NB-IoT is that it should allow reuse of existing hardware due to its backward compatibility with existing 3GPP standards, and can simply ``plug into the LTE core network'' \cite{ratasuk2016nb}. However, the required spectrum costs are a significant entry barrier for network operators, as it requires licensed bands in which to operate. This implies a need for the operator to recoup their investment, meaning that they will necessarily need to charge for access to the radio resource and perform rate-limiting to ensure acceptable quality of service and latency. Therefore, NB-IoT is only likely to be employed in cases where there are strict QoS or latency requirements.

At the time of writing, Vodafone Ireland are currently the only provider of an NB-IoT network in Ireland, since 2017 \cite{vodafone_nbiot_2017}. However, as the underlying protocol is an open standard, other operators with access to the required frequency bands may also offer NB-IoT network access in future. One particular usage of NB-IoT is discussed in \ref{section3.2}.

\section{LP-WAN usage in Ireland}
\label{section3}

\subsection{Flood and Rainfall Monitoring}
\label{section3.1}

As an island recieving a constant flow of damp air from the Gulf Stream, flooding is a recurring issue in Ireland. There have been numerous recent incidents of major flooding due to increased rainfall; the effects of climate change and rising sea levels will only exacerbate this issue in future \cite{murphy2008climate}. Having continuous monitoring in place for flooding events would aid efforts not only in taking emergency action, but also provide additional data on water drainage. LP-WANs would be a key enabling technology for the deployment of sensors to monitor water levels.

In 2016, an experiment was carried out by Dublin City Council (in collaboration with Intel) to utilise a wireless sensor deployment to measure the level of the river Liffey \cite{guibene2017evaluation}. This was mainly a proof-of-concept deployment intended to evaluate the performance of the LoRa physical-layer protocol, and ran for a period of 8 months from January 2016 to August 2016. The experiment was deemed to be a success overall, and successfully demonstrated the use-case for using Wireless Sensor Networks (WSNs) to monitor river levels.

In 2017, the CONNECT centre set up a number of rainfall sensors using the Pervasive Nation LoRaWAN network \cite{doyle_flooding_2018}. A subsequent study \cite{young_river_2019} compared the data collected by these sensors with other data sources (including sensors deployed by Dublin City Council and MET \'{E}ireann). Multiple issues were found, with multiple reports missing from multiple devices, and the values sent by the sensors not correlating with the majority of the rest of the data. It is unclear, however, if the missing values were a result of individual sensors failing or if these were due to underlying issues with the network.

VT also maintain a proof-of-concept deployment of Dunraven Apollo water level sensors connected via the proprietary Sigfox network \cite{vt_flood_monitoring}. These were found to be both more accurate and more reliable than the set of sensors utilising the Pervasive Nation LoRa network \cite{young_river_2019}.

\subsection{Agriculture Technology}
\label{section3.2}

The agricultural sector is one of Ireland's most important industries --- a recent report published by the Irish Government \cite{govie2018agrifacts} claims that the agri-food sector in Ireland accounted for 8.6\% of total employment, and 7.8\% of Gross National Income (GNI) in the year 2016. 

While the na\"{i}ve view of farming is that of a low-tech industry, in reality, farmers are savvy business operators who will happily invest in new technologies that demonstrate tangible time or cost savings in their day-to-day operations. It is no surprise, therefore, that the agri-tech sector has been an area of considerable focus in recent years, with millions of euro of investment in regional Agri-Tech centres of excellence \cite{govie2019swregionenterprise}.

Given the practical considerations of agriculture --- many operations spread around multiple acres of land, not necessarily in one contigious unit --- it would be prohibitively costly to set up any sort of wired network for any sort of data gathering or actuation purposes in this environment. As many agricultural IoT applications would necessarily involve being located outdoors with potentially unreliable GSM network reception, LP-WANs are therefore a key enabling technology for agricultural IoT applications.

Kamilaris et al. \cite{kamilaris2016agri} describe multiple use cases for IoT in the agri-food industries, specifically fertility management for dairy cattle and soil fertility monitoring for crop cultivation. Both of these use cases (the former especially) would necessitate a large number of small unobtrusive sensors connected via LP-WAN. 

Divilly \cite{divilly_factors_2018} performed a survey of Irish farmers' awareness and usage of IoT technologies in their day-to-day operations. Of the valid responses (N=79), approx. 40\% of respondents claimed to be ``Agri-IoT'' users. The users reported a variety of use-cases. Divilly noted two significant LP-WAN use cases represented in the responses:
\begin{itemize}
  \item{
    Calving monitoring systems --- one commercial system in particular named ``MooCall''  utilises the NB-IoT network specifically due to QoS requirements \cite{vodafone2016moocall}, as missing a sensor notification may mean the loss of both cow and calf.
  }
  \item{
    In tillage farming, usage of the Sigfox network (via VT) was noted. Its users specifically cited its wide area coverage and relatively low cost --- one particular use case highlighted was for remote temperature monitoring of a grain silo.
  }
\end{itemize}

Divilly concludes that ease of use is paramount for IoT adoption in the agricultural industry, and cites the example of the calving monitor as an ``off-the-shelf'' system suitably simplified and executed effectively for its target demographic. However, one of the interviews performed is a stark contrast --- the interviewee in this case (Participant D) is obviously an agri-tech enthusiast, and reports building their own agri-IoT systems and ``looking into other 900Mhz transmitters to form a mesh network'', as ``Commerical products are massively overpriced''. It is not inconcievable that other suitably motivated and skilled farmers will attempt to build their own customized IoT systems to fit their own needs, thereby avoiding the OpEx costs associated with commercial IoT systems.

\section{Conclusion}
\label{section4}

There is still significant scope for wider usage of LP-WANs in Ireland. The two examples cited here are not an exhaustive list, however the depth of each of those examples clearly leaves more to be desired. For example, the flood monitoring systems detailed in \ref{section3.1} have only been deployed in the area of County Dublin, while there are significantly more areas affected by flooding on an annual basis. There is significant scope for the Irish Government to invest in LP-WANs for this particular use case.

In the case of the agri-tech sector, there is more significant usage visible, but primarily in the area of commercial systems marketed to farmers as end-users as opposed to system operators. There is still significant scope for innovation and further usage in this area, as evidenced by recent investments in this sector. However, there is still scope for individual farmers to operate their own networks and build their own IoT systems customized to their own individual needs. Additionally, there may be a sizeable niche available for IT professionals with experience in the IoT domain to step into the role of a systems integrator and provide custom IoT solutions to farmers based on open standards such as LoRa.

\label{bibliography}
\bibliographystyle{unsrt}
\bibliography{iot}

\end{document}