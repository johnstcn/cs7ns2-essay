Notes
===

* Types of LPWANs:
    * Proprietary (Sigfox, LoraWAN) vs Open (802.11ah,  NB-IoT)
    * Main focus on power efficiency and high scalability (M2M communications)

Focus on Sigfox, LoRaWAN and NB-IoT

* HYPE:
    * IoT connectivity on the rise according to Vodafone... (https://www.irishtimes.com/business/technology/internet-of-things-adoption-on-the-rise-in-the-republic-1.3867645)
    * Vodafone again: "NB-IoT is [...] designed to wirelessly connect millions of devices" https://www.siliconrepublic.com/machines/debbie-power-vodafone-ireland-iot
    * "...batteries [...] last up to 300x longer" (https://irishtechnews.ie/iot-trends-lpwan-is-something-to-get-very-excited-about-will-ferguson-vtnetworks/)
    * "We plan to connect 1 million sensors [...] by the end of 2017" (https://irishtechnews.ie/iot-trends-lpwan-is-something-to-get-very-excited-about-will-ferguson-vtnetworks/)
    * "Arrival times can be calculated with greater precision based on real-time" (https://www.dachser.ie/en/mediaroom/Connectivity-A-look-at-future-technologies-2279)
    * Types of touted applications:
        * Asset Tracking as a service
        * Flood Warning System
        * Sensor Networks (temperature monitoring)
        * Water Metering
    * Security?

Main hype areas:
    * Power Efficiency
    * Scalability
    * Security

* Reality:
    * Batteries can still fail regardless of how power-efficient your protocol is: "...one of the sensors has run out of battery, with the last contact with the sensor being on the month of February in 2018. [...] All of the devices were installed in November 2017." (Young, 2019)
    * Your sensors may disappear mysteriously: "...the Ballsbridge sensor stopped sending data in the month of November 2018. The reason for this is as yet unknown but due [sic] the tidal activity the owners presume that it may well have been washed away."
    * 