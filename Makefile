all : clean latex

clean :
	rm -f *.pdf *.dvi *.out *.qry *.log *.blg *.bbl *.aux *.bcf *-blx.bib *.run.xml

latex : johnstc-06374611-cs7ns2.tex
	pdflatex johnstc-06374611-cs7ns2.tex
	bibtex johnstc-06374611-cs7ns2.aux
	pdflatex johnstc-06374611-cs7ns2.tex
	pdflatex johnstc-06374611-cs7ns2.tex

